package com.viva.calculator

import android.content.Context

class SaveResult(context: Context){

    val SAVED_RESULT = "savedResult"
    val TEMP_RES = "temporaryResult"


    val preference = context.getSharedPreferences(SAVED_RESULT, Context.MODE_PRIVATE)
    val tempPreference = context.getSharedPreferences(TEMP_RES, Context.MODE_PRIVATE)

    fun getTheSavedResult() : String?{
        return preference.getString(SAVED_RESULT, null)
    }

    fun getTheTempResult() : String?{
        return tempPreference.getString(TEMP_RES, null)
    }

    fun setTheSavedResult(res:String?){
        val editor = preference.edit()
        editor.putString(SAVED_RESULT, res)
        editor.apply()
    }

    fun setTempResult(res:String){
        val editor = tempPreference.edit()
        editor.putString(TEMP_RES, res)
        editor.apply()
    }

    fun deleteResult(){
        preference.edit().remove("savedResult").apply()
    }


}